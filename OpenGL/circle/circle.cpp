#include <windows.h>
#include <gl/gl.h>
#include <glut.h>
#include <math.h>

class GLintPoint
{
public:
	GLint x;
	GLint y;
};

class GLColorf
{
public:
	GLfloat r;
	GLfloat b;
	GLfloat g;
};

GLColorf color = { 1.0f, 0.4f, 1.0f };

void myInit()
{
	glClearColor(0.0, 0.0, 0.0, 0.0);
}

void Reshape(int width, int height) {
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, (GLdouble)width, 0, (GLdouble)height);
}

void putPixel(int x, int y, GLColorf c) {
	glPointSize(1.0f);
	glColor3f(c.r, c.g, c.b);
	glBegin(GL_POINTS);
	glVertex2d(x, y);
	glEnd();
}

void circleSym4(GLintPoint cen, int R, GLColorf c) {
	int r2 = R*R;
	putPixel(cen.x + 0, cen.y + R, c);
	putPixel(cen.x + 0, cen.y - R, c);
	int x, y;
	for (x = 1; x <= R; x++) {
		y = round(pow((r2 - x*x), 0.5));
		putPixel(cen.x + x, cen.y + y, c);
		putPixel(cen.x + x, cen.y - y, c);
		putPixel(cen.x - x, cen.y + y, c);
		putPixel(cen.x - x, cen.y - y, c);
	}
}

void put8Pixels(int x0, int y0, int x, int y, GLColorf c) {
	putPixel(x0 + x, y0 + y, c); putPixel(x0 + x, y0 - y, c);
	putPixel(x0 - x, y0 + y, c); putPixel(x0 - x, y0 - y, c);
	putPixel(x0 + y, y0 + x, c); putPixel(x0 + y, y0 - x, c);
	putPixel(x0 - y, y0 + x, c); putPixel(x0 - y, y0 - x, c);
}

void circleMidPoint(GLintPoint cen, int R, GLColorf c) {
	int x = 0; int y = R;
	put8Pixels(cen.x, cen.y, x, y, c);
	int p = 1 - R;
	while (x < y) {
		if (p < 0) {
			p += 2 * x + 3;
		} else {
			p += 2 * (x - y) + 5;
			y--;
		}
		x++;
		put8Pixels(cen.x, cen.y, x, y, c);
	}
}

void circleSym8(GLintPoint cen, int R, GLColorf c) {
	int r2 = R*R;
	putPixel(cen.x + 0, cen.y + R, c);
	putPixel(cen.x + 0, cen.y - R, c);

	putPixel(cen.x + R, cen.y + 0, c);
	putPixel(cen.x - R, cen.y + 0, c);
	int x = 1, y;
	y = round(pow((r2 - x*x), 0.5));

	for (; x < y;) {

		put8Pixels(cen.x, cen.y, x, y, c);
		/*putPixel(cen.x + x, cen.y - y, c);
		putPixel(cen.x - x, cen.y + y, c); putPixel(cen.x - x, cen.y - y, c);
		putPixel(cen.x + y, cen.y + x, c); putPixel(cen.x + y, cen.y - x, c);
		putPixel(cen.x - y, cen.y + x, c); putPixel(cen.x - y, cen.y - x, c);*/

		x++;
		y = round(pow((r2 - x*x), 0.5));
	}
}

void myDisplay() {
	glClear(GL_COLOR_BUFFER_BIT);

	int width = glutGet(GLUT_WINDOW_WIDTH);
	int height = glutGet(GLUT_WINDOW_HEIGHT);

	GLintPoint center = { width / 2, height / 2 };

	putPixel(center.x, center.y, { 1.0f, 0.0, 0.0 });

	int R1 = 100;
	circleSym4(center, R1, { 1.0f, 0.0, 0.0 });

	int R2 = 200;
	circleSym8(center, R2, { 1.0f, 1.0, 0.0 });
	int R = R1;
	while (R < R2) {
		circleMidPoint(center, R, { 1.0f, 0.0, 1.0 });
		R++;
	}

	int R3 = 300;
	circleMidPoint(center, R3, { 1.0f, 1.0, 1.0 });

	R = R2;
	while (R < R3) {
		circleMidPoint(center, R, { 1.0f, 1.0, 1.0 });
		R++;
	}
	
	glFlush();
}


int main(int argc, char * argv[])
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(640, 480);
	glutInitWindowPosition(100, 150);
	glutCreateWindow("My circle");
	myInit();

	glutDisplayFunc(myDisplay);

	glutReshapeFunc(Reshape);

	glutMainLoop();
	return 0;


}