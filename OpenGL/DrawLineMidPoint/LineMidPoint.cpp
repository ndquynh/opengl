#include <Windows.h>
#include <gl/gl.h>
#include <glut.h>
#include <math.h>


//#define Round(a) int(a+0.5)

struct colorentry {
	unsigned char red;
	unsigned char green;
	unsigned char blue;
	colorentry(int r, int g, int b) :red(r), green(g), blue(b) {};
};

void myInit()
{
	glClearColor(1.0, 1.0, 1.0, 0.0);
	glLineWidth(1.0);
	glPointSize(4.0);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0.0, 400.0, 0.0, 400.0);
}



void PutPixel(int x, int y, colorentry c)
{
	glPointSize(2.0);
	glBegin(GL_POINTS);
	glColor3ub(c.red, c.green, c.blue);
	glVertex2f(x, y);
	glEnd();
}

void LineMid(int x1, int y1, int x2, int y2, colorentry c)
{
	int Dx, Dy, p, Const1, Const2;
	int x, y;
	Dx = x2 - x1;
	Dy = y2 - y1;
	p = 2 * Dy - Dx;
	Const1 = 2 * Dy;
	Const2 = 2 * (Dy - Dx);
	x = x1;
	y = y1;
	PutPixel(x, y, c);
	for (int i = x1; i < x2; i++) {
		if (p < 0) {
			p += Const1;
		}
		else {

			p += Const2;
			y++;

		}
		x++;
		PutPixel(x, y, c);

	}
}

void Display()
{

	glClear(GL_COLOR_BUFFER_BIT);
	LineMid(10, 10, 200, 200, colorentry(255, 0, 255));
	PutPixel(10, 10, colorentry(0, 0, 0));

	glFlush();
}

int main(int argc, char*argv[])
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(640, 480);
	glutInitWindowPosition(100, 150);
	glutCreateWindow("Midpoint");
	glutDisplayFunc(Display);
	myInit();
	glutMainLoop();
	return 0;
}