﻿#include <windows.h>
#include <gl/gl.h>
#include  <glut.h>
#include <math.h>

class MyGLPointInt {
public:
	int x, y;
};

class MyGLPointFloat {
public:
	float x, y;
};


void myInit() {
	glClearColor(1.0, 1.0, 1.0, 1.0); // thiết lập màu nền
	glColor3f(0.0f, 0.0f, 0.0f); // thiết lập màu vẽ
	glPointSize(2.0); // kich thuoc diem ve
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	// câu 1 & 3
	gluOrtho2D(0.0, 50, 0.0, 50); // câu 2
}

int random(int m) {
	return  rand() % m;
}

void drawDot(int x, int y)
{
	glBegin(GL_POINTS);
	glVertex2i(x, y);
	glEnd();
}

void drawPointInt(MyGLPointInt p, float size)
{
	//gluOrtho2D(0.0, 640.0, 0.0, 480.0);
	glPointSize(size); // kich thuoc diem ve
	drawDot(p.x, p.y);
}

/*Cau 2: Ve tam giac */
void sierpinski()
{
	glClear(GL_COLOR_BUFFER_BIT); // xoa màn hình
	glBegin(GL_POINTS);

	MyGLPointInt vertices[3] = { { 10, 10 }, { 250, 400 }, { 500, 10 } };
	int i, j, k;

	MyGLPointInt t = vertices[random(3)];

	glClear(GL_COLOR_BUFFER_BIT);
	glBegin(GL_POINTS);

	for (k = 0; k < 1000000; k++) {
		j = random(3);
		t.x = (t.x + vertices[j].x) / 2;
		t.y = (t.y + vertices[j].y) / 2;

		drawDot(t.x, t.y);
	}
	glEnd();
	glFlush();
}

void drawLineInt(MyGLPointInt p1, MyGLPointInt p2, GLfloat lineWidth) {
	gluOrtho2D(0.0, 640.0, 0.0, 480.0);
	glLineWidth(lineWidth);// set befor begin to draw
	glBegin(GL_LINES);
	glVertex2i(p1.x, p1.y);
	glVertex2i(p2.x, p2.y);
	glEnd();
}

void init()
{
	glClearColor(0.0, 0.5, 0.2, 0.8);
}

int main(int argc, char* argv[])
{
	glutInit(&argc, argv); // khởi tạo tool kit
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);//thiết lập chế độ màn hình
	glutInitWindowSize(800, 800); //thiết lập kích thước cửa sổ
	glutInitWindowPosition(100, 150); // thiết lập vị trí cửa sổ
	glutCreateWindow("Nguyen Duc Quynh"); // mở cửa sổ màn hình
	glutDisplayFunc(sierpinski); // đăng ký hàm vẽ lại
	myInit();
	glutMainLoop(); //lặp vô tận

	return 0;
}
