#include <windows.h>
#include <gl/gl.h>
#include  <glut.h>
#include <math.h>

class GLintPoint
{
public:
	GLint x;
	GLint y;
};

GLintPoint CP;


class GLColorf
{
public:
	GLfloat r;
	GLfloat b;
	GLfloat g;
};

GLColorf color = {1.0f, 0.4f, 1.0f};

void moveto(GLint x, GLint y)
{
	CP.x = x;
	CP.y = y;
}

void myInit()
{
	glClearColor(1.0, 1.0, 1.0, 0.0);
	glColor3f(0.0f, 0.0f, 0.0f);
	glPointSize(4.0);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0.0, 640.0, 0.0, 480.0);
}

void myDisplay()
{
	GLintPoint A = { 100, 100 };
	GLintPoint B = { 300, 400 };

	glClear(GL_COLOR_BUFFER_BIT);
	//glRecti(A.x, A.y, B.x, B.y);
	glColor3f(0.0f, 1.0f, 1.0f);
	GLintPoint C = { 400, 300 };
	GLintPoint D = { 200, 200 };
	GLintPoint E = { 200, 300 };

	glBegin(GL_POLYGON);
	glVertex2i(A.x, A.y);
	glVertex2i(B.x, B.y);
	glVertex2i(C.x, C.y);
	glVertex2i(D.x, D.y);
	glVertex2i(E.x, E.y);

	glEnd();

	glFlush();
}

void drawTriangular(GLintPoint A, GLintPoint B, GLintPoint C, GLColorf c) {
	glColor3f(c.r, c.b, c.g);
	glBegin(GL_TRIANGLES);
		glVertex2i(A.x, A.y);
		glVertex2i(B.x, B.y);
		glVertex2i(C.x, C.y);
	glEnd();
	glFlush();
}

void drawQuard(GLintPoint A, GLintPoint B, GLintPoint C, GLintPoint D, GLColorf c) {
	glColor3f(c.r, c.b, c.g);
	glBegin(GL_QUADS);
		glVertex2i(A.x, A.y);
		glVertex2i(B.x, B.y);
		glVertex2i(C.x, C.y);
		glVertex2i(D.x, D.y);
	glEnd();
	glFlush();
}

void drawHexagon(GLintPoint center, GLint length, GLColorf c) {
	glColor3f(c.r, c.b, c.g);
	glBegin(GL_TRIANGLE_FAN);


	
	glEnd();
	glFlush();
}


void myTriangle() {
	GLintPoint A = { 100, 100 };
	GLintPoint B = { 300, 400 };

	GLintPoint C = { 400, 300 };

	GLintPoint E = { 200, 300 };
	
	color.r = 1.0f;

	drawTriangular(A, E, C, color);
}

void myQuard() {
	GLintPoint A = { 100, 100 };
	GLintPoint B = { 200, 100 };
	GLintPoint C = { 200, 300 };
	GLintPoint E = { 100, 300 };

	color.r = 1.0f;

	drawQuard(A, B, C, E, color);
}


int main(int argc, char * argv[])
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(640, 480);
	glutInitWindowPosition(100, 150);
	glutCreateWindow("My first program");
	glutDisplayFunc(myQuard);
	myInit();
	glutMainLoop();
	return 0;


}