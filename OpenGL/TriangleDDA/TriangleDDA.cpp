﻿#include <windows.h>
#include <gl/gl.h>
#include <glut.h>
#include <math.h>
#include <iostream>
#include <vector>
#define sreenHeight 480
#define sreenWidth 640
#define Round(a) int(a+0.5)

using namespace std;

struct colorentry {
	unsigned char red;
	unsigned char green;
	unsigned char blue;
	colorentry(int r, int g, int b) :red(r), green(g), blue(b) {};
};

class GLintPoint
{
public:
	GLint x, y;
};


// ham ve diem
void PutPixel(GLint x, GLint y, colorentry c)
{
	glPointSize(2.0);
	glBegin(GL_POINTS);
	glColor3ub(c.red, c.green, c.blue);
	glVertex2f(x, y);
	glEnd();
	glFlush();
}
/////////////////

// ve duong thang
void  LineDDA(GLint x1, GLint y1, GLint x2, GLint y2, colorentry c)
{
	int xbd, ybd, xkt, ykt, x, i;
	if (x1 > x2)
	{
		xbd = x2;
		ybd = y2;
		xkt = x1;
		ykt = y1;

	}
	if (x1 <= x2)
	{
		xbd = x1;
		ybd = y1;
		xkt = x2;
		ykt = y2;

	}
	x = xbd;
	float y = ybd;
	float m = float(y2 - y1) / (x2 - x1);
	PutPixel(x, round(y), c);
	for (int i = xbd; i < xkt; i++)
	{
		x++;
		y += m;
		PutPixel(x, Round(y), c);

	}
}

void tamgia(GLint x1, GLint y1, GLint x2, GLint y2, GLint x3, GLint y3, colorentry c)
{
	LineDDA(x1, y1, x2, y2, c);
	LineDDA(x2, y2, x3, y3, c);
	LineDDA(x1, y1, x3, y3, c);
}
/////////////////

// hinh chu nhat
void  LineDDA1(int x1, int y1, int x2, int y2, colorentry c)
{
	int xbd, ybd, xkt, ykt, x, i;
	if (x1 > x2)
	{
		xbd = x2;
		ybd = y2;
		xkt = x1;
		ykt = y1;

	}
	if (x1 < x2)
	{
		xbd = x1;
		ybd = y1;
		xkt = x2;
		ykt = y2;

	}
	x = xbd;
	float y = ybd;
	float m = float(y2 - y1) / (x2 - x1);
	PutPixel(x, round(y), c);
	for (int i = xbd; i < xkt; i++)
	{
		x++;
		y += m;
		PutPixel(x, Round(y), c);

	}

}

void  LineDDA2(int x1, int y1, int x2, int y2, colorentry c)
{
	int xbd, ybd, xkt, ykt, i;
	if (y1 > y2)
	{
		xbd = x2;
		ybd = y2;
		xkt = x1;
		ykt = y1;
	}
	else

	{
		xbd = x1;
		ybd = y1;
		xkt = x2;
		ykt = y2;
	}

	int y = ybd;
	float x = xbd;
	float chia_m = float(x2 - x1) / (y2 - y1);
	PutPixel(Round(x), y, c);
	for (i = ybd; i < ykt; i++)
	{
		y++;
		x += chia_m;
		PutPixel(Round(x), y, c);
	}
}

void  LineDDA3(int x1, int y1, int x2, int y2, colorentry c)
{
	int ybd = (y1>y2) ? y2 : y1;
	int ykt = (y1 > y2) ? y1 : y2;
	for (int y = ybd; y < ykt; y++)
	{
		PutPixel(x1, y, c);
		y++;
	}
}

void  LineDDA4(int x1, int y1, int x2, int y2, colorentry c)
{
	int xbd = (x1>x2) ? x2 : x1;
	int xkt = (x1 > x2) ? x1 : x2;
	for (int x = xbd; x < xkt; x++)
	{
		PutPixel(x, y1, c);
		x++;
	}
}

void LineDDAHCN(int x1, int y1, int x2, int y2, colorentry c)
{
	int dx = abs(x2 - x1);
	int dy = abs(y2 - y1);
	if (dx > dy)

		LineDDA1(x1, y1, x2, y2, c);

	else if (dx < dy)


		LineDDA2(x1, y1, x2, y2, c);

	else if (dx == 0)

		LineDDA4(x1, y1, x2, y2, c);

	else if (dy == 0)

		LineDDA3(x1, y1, x2, y2, c);
}

vector<GLintPoint> hinhchunhat(GLint x1, GLint y1, GLint x2, GLint y2, colorentry c)
{
	vector<GLintPoint> corners(5);
	
	corners[0] = { x1 , y1 };
	corners[1] = { x1 , y2 };
	corners[2] = { x2 , y2 };
	corners[3] = { x2 , y1 };
	corners[4] = { x1 , y1 };

	//LineDDAHCN(x1, y1, x1, y2, colorentry(255, 0, 0));
	//LineDDAHCN(x1, y2, x2, y2, colorentry(255, 0, 0));
	//LineDDAHCN(x2, y2, x2, y1, colorentry(255, 0, 0));
	//LineDDAHCN(x2, y1, x1, y1, colorentry(255, 0, 0));

	LineDDAHCN(corners[0].x, corners[0].y, corners[1].x, corners[1].y, colorentry(255, 0, 0));
	LineDDAHCN(corners[1].x, corners[1].y, corners[2].x, corners[2].y, colorentry(255, 0, 0));
	LineDDAHCN(corners[2].x, corners[2].y, corners[3].x, corners[3].y, colorentry(255, 0, 0));
	LineDDAHCN(corners[3].x, corners[3].y, corners[0].x, corners[0].y, colorentry(255, 0, 0));
	
	return corners;
}
/////////////////

void ScanLine(vector<GLintPoint> p, int v, colorentry color)
{
	int xmin, xmax, ymin, ymax, c, mang[50];
	xmin = xmax = p[0].x;
	ymin = ymax = p[0].y;
	for (int i = 0; i < v; i++) {
		if (xmin > p[i].x) xmin = p[i].x;
		if (xmax < p[i].x) xmax = p[i].x;
		if (ymin > p[i].y) ymin = p[i].y;
		if (ymax < p[i].y) ymax = p[i].y;
	}

	float y;
	y = ymin + 1;
	while (y <= ymax) { // với y tăng dần từ ymin > ymax,tìm các giao điểm của từng y với các cặp cạnh

		cout << y << " < " << ymax << endl;

		int x, x1, x2, y1, y2, tg;
		c = 0;    //chỉ số của mảng phần tử
		for (int i = 0; i < v; i++) { //xét trên tất cả các đỉnh
									  //xét 2 đỉnh liền kề nhau
			x1 = p[i].x;
			y1 = p[i].y;
			x2 = p[i + 1].x;
			y2 = p[i + 1].y;
			if (y2 < y1) { //sắp xếp lại y của 2 điểm liên tiếp
				tg = x1; x1 = x2; x2 = tg;
				tg = y1; y1 = y2; y2 = tg;
			}
			//mảng giao điểm
			if (y <= y2 && y >= y1) {
				if (y1 == y2)  x = x1; //nếu y của 2 đỉnh liên tiếp trùng nhau => bỏ qua
				else {
					x = ((y - y1)*(x2 - x1)) / (y2 - y1); //hệ số góc
					x += x1; //300
				}
				if (x <= xmax && x >= xmin)
					mang[c++] = x;   //cho phần tử c = x sau đó c++
			}
		}
		if (c < 2) {
			y++;
			continue;
		}
		//với từng y tăng dần ta vẽ luôn đường thằng nối 2 giao điểm
		for (int i = 0; i < c; i += 2) {
			//delay(30);
			if (i + 1 >= c)
				break;
			LineDDA((int)mang[i], (int)y, (int)mang[i + 1], (int)y, color);
		}   //line(302,91,300,91
		y++;
	}
}

void myInit()
{
	glClearColor(1.0, 1.0, 1.0, 1.0);
	glColor3f(0.0f, 0.0f, 1.0f);
	glPointSize(3.0);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0.0, sreenWidth, 0.0, sreenHeight);
}

void myDisplay()
{
	glClear(GL_COLOR_BUFFER_BIT);
	int xMax = glutGet(GLUT_WINDOW_WIDTH);
	int yMax = glutGet(GLUT_WINDOW_HEIGHT);
	glFlush();
}

void myMouse(int button, int state, int x, int y)
{

	// ve hinh chu nhat
	static GLintPoint cornerHCN[2];
	vector<GLintPoint> realConers;

	static int numCornersHCN = 0;
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
	{
		cornerHCN[numCornersHCN].x = x;
		cornerHCN[numCornersHCN].y = sreenHeight - y;
		numCornersHCN++;
		if (numCornersHCN == 2)
		{
			realConers = hinhchunhat(cornerHCN[0].x, cornerHCN[0].y, cornerHCN[1].x, cornerHCN[1].y, colorentry(255, 0, 255));
			ScanLine(realConers, 4, colorentry(255, 0, 255));
			numCornersHCN = 0;
		}
	}
	else if (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN)
	{
		glClear(GL_COLOR_BUFFER_BIT);
		numCornersHCN = 0;
	}


}

int main(int argc, char* argv[])
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(sreenWidth, sreenHeight);
	glutInitWindowPosition(100, 150);
	glutCreateWindow("my Mouse");
	glutDisplayFunc(myDisplay);
	glutMouseFunc(myMouse);
	myInit();
	glutMainLoop();
	return 0;
}

