﻿#include <windows.h>
#include <gl/gl.h>
#include  <glut.h>
#include <math.h>

class GLintPoint
{
public:
	GLint x;
	GLint y;
};

GLintPoint CP;

void moveto(GLint x, GLint y)
{
	CP.x = x;
	CP.y = y;
}

void lineto(GLint x, GLint y)
{
	glBegin(GL_LINES);
	glVertex2i(CP.x, CP.y);
	glVertex2i(x, y); glEnd();
	glFlush();
	CP.x = x; CP.y = y;
}
void myInit()
{
	glClearColor(1.0, 1.0, 1.0, 0.0);
	glColor3f(0.0f, 0.0f, 0.0f);
	glPointSize(4.0);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0.0, 640.0, 0.0, 480.0);
}

void myDisplay()
{
	int x1 = 100, y1 = 100;
	int x2 = 300, y2 = 400;

	glClear(GL_COLOR_BUFFER_BIT);
	moveto(x1, y1);
	lineto(x2, y2);

	glFlush();
}
int main(int argc, char * argv[])
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(640, 480);
	glutInitWindowPosition(100, 150);
	glutCreateWindow("My first program");
	glutDisplayFunc(myDisplay);
	myInit();
	glutMainLoop();
	return 0;


}