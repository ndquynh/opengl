
#include <math.h>
#include <queue>
#include <glut.h>

using namespace std;

// Cau truc diem
struct Point {
	GLint x;
	GLint y;
};

// Khai bao cau truc mau sac
struct Color {
	GLfloat r;
	GLfloat g;
	GLfloat b;
};

// Dinh nghia toa do bat dau
Point pStart = { 320, 240 };
// Chieu dai 
GLfloat WIDTH = 100;
// Chieu cao
GLfloat HEIGHT = 50;

GLint my_menu;
// Dinh nghia toa do tam hinh tron
Point I = { 320, 240 };
// Dinh nghia ban kinh hinh tron
GLfloat R = 50;



/* Ham khoi tao*/
void init() {
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glColor3f(1.0, 1.0, 1.0);
	glPointSize(1.0);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, 640, 0, 480);
}

/* Lay mau tai toa do bat ki */
Color getPixelColor(GLint x, GLint y) {
	Color color;
	glReadPixels(x, y, 1, 1, GL_RGB, GL_FLOAT, &color);
	return color;
}

/* Set mau tai toa do bat ki */
void setPixelColor(GLint x, GLint y, Color color) {
	glColor3f(color.r, color.g, color.b);
	glBegin(GL_POINTS);
	glVertex2i(x, y);
	glEnd();
	glFlush();
}
/* Kiem tra diem co nam trong hinh tron khong*/
bool isPointInsideSquare(Point p) {
	return (p.x < pStart.x && p.x < pStart.x + WIDTH)
		&& (p.y < pStart.y && p.y < pStart.y + HEIGHT);
}

/*So sanh hai mau*/
bool compareColor(Color c1, Color c2) {
	return (c1.r == c2.r && c1.g == c2.g && c1.b == c2.b);
}
/*Ham to mau*/
void fillColor(Point p, Color oldColor, Color newColor)
{
	Color color = getPixelColor(p.x, p.y);
	//  Khai bao queue chua pixel chua duoc to mau
	queue<Point> Q;
	Point m, Tg;
	if (compareColor(color, oldColor))
	{
		m.x = p.x;
		m.y = p.y;
		setPixelColor(m.x, m.y, newColor);
		Q.push(m);  //  Them 1 diem vao queue, queue size tang 1
		while (Q.empty() == false)   //Xet 4 diem xung quanh voi moi diem luu trong queue (neu queue con phan tu)
		{
			Q.pop();//  Xoa 1 diem phia dau queue, queue size giam 1
					//Xet cac diem lan can cua 1 diem
			color = getPixelColor(m.x + 1, m.y);
			if (compareColor(color, oldColor))
			{
				setPixelColor(m.x + 1, m.y, newColor);
				Tg.x = m.x + 1;
				Tg.y = m.y;
				Q.push(Tg);// Them 1 diem vao cuoi queue
			}
			color = getPixelColor(m.x - 1, m.y);
			if (compareColor(color, oldColor))
			{
				setPixelColor(m.x - 1, m.y, newColor);
				Tg.x = m.x - 1;
				Tg.y = m.y;
				Q.push(Tg);
			}
			color = getPixelColor(m.x, m.y + 1);
			if (compareColor(color, oldColor))
			{
				setPixelColor(m.x, m.y + 1, newColor);
				Tg.x = m.x;
				Tg.y = m.y + 1;
				Q.push(Tg);
			}
			color = getPixelColor(m.x, m.y - 1);
			if (compareColor(color, oldColor))
			{
				setPixelColor(m.x, m.y - 1, newColor);
				Tg.x = m.x;
				Tg.y = m.y - 1;
				Q.push(Tg);
			}
			if (Q.empty() == false) {
				m = Q.front();// Dua ve gia tri dau tien cho hang doi
			}
		}
	}
}

/* Ham ve doan thang*/
void drawLineBresenham(int x1, int y1, int x2, int y2)
{
	glBegin(GL_POINTS);
	int c2, c, Dx, Dy, x, y;
	Dx = abs(x2 - x1);
	Dy = abs(y2 - y1);
	c = Dx - Dy;
	c2 = 2 * c;
	x = x1;
	y = y1;

	int x_unit = 1, y_unit = 1;

	if (x2 - x1 < 0)
		x_unit = -x_unit;
	if (y2 - y1 < 0)
		y_unit = -y_unit;

	glVertex3f(x, y, 0);

	if (x1 == x2)   // duong thang dung
	{
		while (y != y2)
		{
			y += y_unit;
			glVertex3f(x, y, 0);
		}
	}

	else if (y1 == y2)  // duong ngang
	{
		while (x != x2)
		{
			x += x_unit;
			glVertex3f(x, y, 0);
		}
	}

	else if (x1 != x2 && y1 != y2)  // duong xien
	{
		while (x != x2 + 1)
		{
			c2 = 2 * c;
			if (c2 > -Dy)
			{
				c = c - Dy;
				x = x + x_unit;
			}
			if (c2 < Dx)
			{
				c = c + Dx;
				y = y + y_unit;
			}
			glVertex3f(x, y, 0);
		}
	}

}

/* Ve hinh tron*/
void drawCircle() {
	glClear(GL_COLOR_BUFFER_BIT);
	glBegin(GL_POINTS);
	GLfloat step = 1 / R;
	GLfloat x, y;

	for (GLfloat theta = 0; theta <= 360; theta += step) {
		x = I.x + (R * cos(theta));
		y = I.y + (R * sin(theta));
		glVertex2i(x, y);
	}
	glEnd();
	glFlush();
}
/* Ve hinh vuong*/
void drawSquare() {
	glClear(GL_COLOR_BUFFER_BIT);
	glBegin(GL_POINTS);
	drawLineBresenham(pStart.x, pStart.y, pStart.x, pStart.y + WIDTH);
	drawLineBresenham(pStart.x, pStart.y, pStart.x + WIDTH, pStart.y);
	drawLineBresenham(pStart.x, pStart.y + WIDTH, pStart.x + WIDTH, pStart.y + WIDTH);
	drawLineBresenham(pStart.x + WIDTH, pStart.y, pStart.x + WIDTH, pStart.y + WIDTH);
	glEnd();
	glFlush();
}
/* Ve hinh chu nhat*/
void drawRectangle() {
	glClear(GL_COLOR_BUFFER_BIT);
	glBegin(GL_POINTS);
	drawLineBresenham(pStart.x, pStart.y, pStart.x, pStart.y + HEIGHT);
	drawLineBresenham(pStart.x, pStart.y, pStart.x + WIDTH, pStart.y);
	drawLineBresenham(pStart.x, pStart.y + HEIGHT, pStart.x + WIDTH, pStart.y + HEIGHT);
	drawLineBresenham(pStart.x + WIDTH, pStart.y, pStart.x + WIDTH, pStart.y + HEIGHT);
	glEnd();
	glFlush();
}
/* Ve hinh vuong*/
void drawTriangle() {
	glClear(GL_COLOR_BUFFER_BIT);
	glBegin(GL_POINTS);
	drawLineBresenham(pStart.x, pStart.y, pStart.x + WIDTH, pStart.y);
	drawLineBresenham(pStart.x, pStart.y, pStart.x + WIDTH / 3, pStart.y + WIDTH);
	drawLineBresenham(pStart.x + WIDTH, pStart.y, pStart.x + WIDTH / 3, pStart.y + WIDTH);
	glEnd();
	glFlush();
}
void drawPoint(Point p) {
	glVertex3f(p.x, p.y, 0);
}

/* Hien thi khung nhin*/
void display(void) {
	glClear(GL_COLOR_BUFFER_BIT);
	glBegin(GL_POINTS);
	glEnd();
	glFlush();
}
void drawImage(int value) {
	switch (value) {
	case 1:
	{
		drawCircle();
		Color newColor = { 1.0f, 0.0f, 0.0f }; // Mau nen
		Color oldColor = { 0.0f, 0.0f, 0.0f }; // Mau muon to
		Point p = { I.x,I.y };
		fillColor(p, oldColor, newColor); // THuc hien to mau hinh tron

	}
	break;
	case 2:
	{
		drawSquare();
		Color newColor = { 0.0f, 0.0f, 1.0f }; // Mau nen
		Color oldColor = { 0.0f, 0.0f, 0.0f }; // Mau muon to
		Point p = { pStart.x + 1, pStart.y + 1 };
		fillColor(p, oldColor, newColor); // THuc hien to mau hinh tron
	}
	break;
	case 3:
	{
		drawRectangle();
		Color newColor = { 0.0f, 1.0f, 0.0f }; // Mau nen
		Color oldColor = { 0.0f, 0.0f, 0.0f }; // Mau muon to
		Point p = { pStart.x + 1,pStart.y + 1 };
		fillColor(p, oldColor, newColor); // THuc hien to mau hinh tron
	}
	break;
	case 4:
	{
		drawTriangle();
		Color newColor = { 1.0f, 1.0f, 0.0f }; // Mau nen
		Color oldColor = { 0.0f, 0.0f, 0.0f }; // Mau muon to
		Point p = { pStart.x + 1,pStart.y + 1 };
		fillColor(p, oldColor, newColor); // THuc hien to mau hinh tron
	}
	break;
	}
}
/* Ham click vao ve 1 diem*/
void myMouse(int button, int state, int x, int y)
{
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
		glBegin(GL_POINTS);
		glColor3f(1.0, 1.0, 1.0);
		Point p = { x, 480 - y };
		drawPoint(p);
		glEnd();
		glFlush();

	}
}


/*Ham xu ly chinh*/
int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(640, 480);
	glutInitWindowPosition(200, 200);
	glutCreateWindow("BT_OPENGL");
	init();
	glutDisplayFunc(display);
	glutMouseFunc(myMouse);
	my_menu = glutCreateMenu(drawImage);
	glutAddMenuEntry("Ve Hinh Tron", 1);
	glutAddMenuEntry("Ve Hinh Vuong", 2);
	glutAddMenuEntry("Ve Hinh Chu Nhat", 3);
	glutAddMenuEntry("Ve Hinh Tam Giac", 4);
	glutAttachMenu(GLUT_RIGHT_BUTTON);
	glutMainLoop();
	return 0;
}