﻿#include <windows.h>
#include <gl/gl.h>
#include  <glut.h>
#include <math.h>
#include <iostream>
using namespace std;


class GLintPoint
{
public:
	GLint x;
	GLint y;
};

class GLColorf
{
public:
	GLfloat r;
	GLfloat b;
	GLfloat g;
};

GLColorf color = { 1.0f, 1.0f, 1.0f };

const int screenWidth = 640;
const int screenHigh = 480;
const int screenPosX = 200;
const int screenPosY = 200;

void myInit()
{
	glClearColor(0.0, 0.0, 0.0, 0.0);
}

void Reshape(int width, int height) {
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, (GLdouble)width, 0, (GLdouble)height);
}

void putPixel(int x, int y, GLColorf c) {
	glPointSize(4.0f);
	glColor3f(c.r, c.g, c.b);
	glBegin(GL_POINTS);
	glVertex2d(x, y);
	glEnd();

	glFlush();
}

void myDisplay() {
	glClear(GL_COLOR_BUFFER_BIT);

	glFlush();
}

void lineUpper(GLintPoint A, GLintPoint B, GLColorf c) {
	int Dx, Dy, p;
	int x, y;
	int step = 1;

	Dx = B.x - A.x;
	Dy = B.y - A.y;

	if (Dx < 0) {
		step = -1;
		Dx = -Dx;
	}

	p = 2 * Dx - Dy;

	x = A.x;

	for (y = A.y; y < B.y; y++)
	{
		putPixel(x, y, c);
		if (p > 0)
		{
			x += step;
			p = p - 2 * Dy;
		}
		p += 2 * Dx;
	}

}

void lineDownLow(GLintPoint A, GLintPoint B, GLColorf c) {
	int Dx, Dy, p;
	int x, y;
	int step = 1;

	Dx = B.x - A.x;
	Dy = B.y - A.y;

	if (Dy < 0) {
		step = -1;
		Dy = -Dy;
	}

	p = 2 * Dy - Dx;
	y = A.y;

	for (x = A.x; x < B.x; x++)
	{
		putPixel(x, y, c);
		if (p > 0)
		{
			y += step;
			p = p - 2 * Dx;
		}
		p += 2 * Dy;
	}
}

void LineMidPoint(GLintPoint A, GLintPoint B, GLColorf c)
{
	int Dx, Dy;

	Dx = B.x - A.x;
	Dy = B.y - A.y;

	if (abs(Dy) < abs(Dx)) {
		if (Dx < 0) {
			lineDownLow(B, A, c);
		}
		else
		{
			lineDownLow(A, B, c);
		}
	}
	else
	{
		if (Dy < 0) {
			lineUpper(B, A, c);
		}
		else
		{
			lineUpper(A, B, c);
		}
	}
}

void ScanLine(GLintPoint p[], int v, GLColorf color)
{
	int xmin, xmax, ymin, ymax, c, mang[50];
	xmin = xmax = p[0].x;
	ymin = ymax = p[0].y;
	for (int i = 0; i < v; i++) {
		if (xmin > p[i].x) xmin = p[i].x;
		if (xmax < p[i].x) xmax = p[i].x; 
		if (ymin > p[i].y) ymin = p[i].y;
		if (ymax < p[i].y) ymax = p[i].y;
	}

	/*ymin = min(min(p[0].y, p[1].y), min(p[1].y, p[2].y));
	int midY = max(min(p[0].y, p[1].y), min(max(p[0].y, p[1].y), p[2].y));
	ymax = max(max(p[0].y, p[1].y), max(p[1].y, p[2].y));*/

	float y;
	y = ymin + 1;
	while (y <= ymax) { // với y tăng dần từ ymin > ymax,tìm các giao điểm của từng y với các cặp cạnh
		
		cout << y << " < " << ymax << endl;

		/*if (y + 1 == midY) {
			cout << y << " < mid " << midY << endl;
		}*/

		int x, x1, x2, y1, y2, tg;
		c = 0;    //chỉ số của mảng phần tử
		for (int i = 0; i < v; i++) { //xét trên tất cả các đỉnh
									//xét 2 đỉnh liền kề nhau
			x1 = p[i].x;
			y1 = p[i].y;
			x2 = p[i + 1].x;
			y2 = p[i + 1].y;
			if (y2 < y1) { //sắp xếp lại y của 2 điểm liên tiếp
				tg = x1; x1 = x2; x2 = tg;
				tg = y1; y1 = y2; y2 = tg;
			}
			//mảng giao điểm
			if (y <= y2 && y >= y1) {
				if (y1 == y2)  x = x1; //nếu y của 2 đỉnh liên tiếp trùng nhau => bỏ qua
				else {
					x = ((y - y1)*(x2 - x1)) / (y2 - y1); //hệ số góc
					x += x1; //300
				}
				if (x <= xmax && x >= xmin)
					mang[c++] = x;   //cho phần tử c = x sau đó c++
			}
		}
		if (c < 2) {
			y++;
			continue;
		}
		//với từng y tăng dần ta vẽ luôn đường thằng nối 2 giao điểm
		for (int i = 0; i < c; i += 2) {
			//delay(30);
			if (i + 1 >= c)
				break;
			LineMidPoint({ (int)mang[i] , (int)y }, { (int)mang[i + 1], (int)y }, color);
		}   //line(302,91,300,91
		y++;
	}
}

int clickCounter = 0;
GLintPoint point1;
GLintPoint point2;

void myMouseFuncLine(int button, int state, int x, int y)
{
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
		clickCounter++;
		if (clickCounter % 2 == 0) {
			point2 = { x, screenHigh - y };
			LineMidPoint(point1, point2, color);
			clickCounter--;
			point1 = point2;
		}
		else {
			point1 = { x, screenHigh - y };
			putPixel(point1.x, point1.y, color);
		}
	}
	else if (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN) {
		exit(-1);
	}
}

GLintPoint triangle[3] ;
void myMouseFuncTriangle(int button, int state, int x, int y)
{

	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
		
		if (clickCounter > 2) {
			return;
		}

		if (clickCounter >= 1) {
			if (triangle[clickCounter - 1].x == x && triangle[clickCounter - 1].y == screenHigh - y) {
				return;
			}
		}

		triangle[clickCounter] = { x, screenHigh - y };
		putPixel(triangle[clickCounter].x, triangle[clickCounter].y, color);

		if (clickCounter >= 1) {
			
			LineMidPoint(triangle[clickCounter - 1], triangle[clickCounter], color);
		}

		//if (clickCounter >= 3) {
		//	LineMidPoint(triangle[clickCounter - 1], triangle[0], color);
		//}

		if (clickCounter >= 2) {
			LineMidPoint(triangle[clickCounter], triangle[clickCounter - 2], color);
		}
		
		++clickCounter;
	}
	else if (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN) {
		//exit(-1);
		//fillScanLineTriangle(triangle, {1.0f, 0.0, 0.0});
		triangle[clickCounter] = triangle[0];
		ScanLine(triangle, 3, { 1.0f, 0.0, 0.0 });
		clickCounter = 0;
	}
}



void myMouseFuncRect(int button, int state, int x, int y)
{

	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
		clickCounter++;

		if (clickCounter % 2 == 0) {
			point2 = { x, screenHigh - y };
			LineMidPoint(point1, point2, color);
			clickCounter--;
			point1 = point2;
		}
		else 
		{
			point1 = { x, screenHigh - y };
			putPixel(point1.x, point1.y, color);
		}


	}
	else if (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN) {
		exit(-1);
	}
}

//
//int main(int argc, char * argv[]) {
//
//	glutInit(&argc, argv);
//	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
//	glutInitWindowSize(screenWidth, screenHigh);
//	glutInitWindowPosition(screenPosX, screenPosY);
//	glutCreateWindow("draw line Color");
//
//	myInit();
//
//	glutDisplayFunc(myDisplay);
//
//	glutReshapeFunc(Reshape);
//
//	glutMouseFunc(myMouseFuncTriangle);
//
//	glutMainLoop();
//
//	return 0;
//}