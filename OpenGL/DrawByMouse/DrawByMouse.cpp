#include <windows.h>
#include <gl/gl.h>
#include  <glut.h>
#include <math.h>


class GLintPoint
{
public:
	GLint x;
	GLint y;
};

class GLColorf
{
public:
	GLfloat r;
	GLfloat b;
	GLfloat g;
};

GLColorf color = { 1.0f, 0.4f, 1.0f };

const int screenWidth = 640;
const int screenHigh = 480;

void myInit()
{
	glClearColor(0.0, 0.0, 0.0, 0.0);
}

void Reshape(int width, int height) {
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, (GLdouble)width, 0, (GLdouble)height);
}

void putPixel(int x, int y, GLColorf c) {
	glPointSize(4.0f);
	glColor3f(c.r, c.g, c.b);
	glBegin(GL_POINTS);
	glVertex2d(x, y);
	glEnd();
	
	glFlush();
}
 
void myDisplay() {
	glClear(GL_COLOR_BUFFER_BIT);

	glFlush();
}

void myMouseFunc(int button, int state, int x, int y)
{
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
		putPixel(x, screenHigh - y, color);
	else if (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN)
		exit(-1);
}

int main(int argc, char * argv[])
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(screenWidth, screenHigh);
	glutInitWindowPosition(100, 150);
	glutCreateWindow("draw dot");
	myInit();

	glutDisplayFunc(myDisplay);

	glutReshapeFunc(Reshape);

	glutMouseFunc(myMouseFunc);

	glutMainLoop();
	return 0;


}