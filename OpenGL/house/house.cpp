﻿#include <windows.h>
#include <gl/gl.h>
#include  <glut.h>
#include <math.h>

class MyGLPointInt {
public:
	int x, y;
};

class MyGLPointFloat {
public:
	float x, y;
};


void myInit() {
	glClearColor(1.0, 1.0, 1.0, 1.0); // thiết lập màu nền
	glColor3f(0.0f, 0.0f, 0.0f); // thiết lập màu vẽ
	glPointSize(2.0); // kich thuoc diem ve
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	// câu 1 & 3
	gluOrtho2D(0.0, 50, 0.0, 50); // câu 2
}

void drawLineInt(MyGLPointInt p1, MyGLPointInt p2, GLfloat lineWidth) {
	gluOrtho2D(0.0, 640.0, 0.0, 480.0);
	glLineWidth(lineWidth);// set befor begin to draw
	glBegin(GL_LINES);
	glVertex2i(p1.x, p1.y);
	glVertex2i(p2.x, p2.y);
	glEnd();
}

void drawHouse() {
	int wallHeight = 250;
	int houseWidth = 400;
	int roofTopHeight = 350;

	glClear(GL_COLOR_BUFFER_BIT);
	MyGLPointInt a = { 100, 100 };
	MyGLPointInt b = { a.x + houseWidth, 100 };

	GLfloat wallThick = 4.0F;

	// Draw base floor
	drawLineInt(a, b, wallThick);

	// Draw walls
	glBegin(GL_LINE_STRIP);

	glColor3f(1.0, 0.0, 0.0);
	glVertex3i(a.x, a.y, 0.0);
	glVertex3i(a.x, a.y + wallHeight, 0.0);

	//Draw roof
	glColor3f(1.0, 0.0, 0.0);
	glVertex3i((a.x + b.x) / 2, b.y + roofTopHeight, 0.0);
	glVertex3i(b.x, b.y + wallHeight, 0.0);

	// Draw right wall
	glVertex3i(b.x, b.y, 0.0);
	glEnd();


	// Door
	glBegin(GL_LINE_STRIP);

	glColor3f(1.0, 0.0, 1.0);
	glVertex3i(a.x + (b.x - a.x) / 4, a.y, 0.0);
	glVertex3i(a.x + (b.x - a.x) / 4, a.y + wallHeight / 2, 0.0);
	glVertex3i(a.x + 2 * (b.x - a.x) / 4, a.y + wallHeight / 2, 0.0);
	glVertex3i(a.x + 2 * ((b.x - a.x) / 4), a.y, 0.0);
	glEnd();
	glFlush();

	//window
	glBegin(GL_LINE_LOOP);
	int windowWidth = (b.x - (b.x - a.x) / 8);
	glVertex3i(b.x - (b.x - a.x) / 8, b.y + wallHeight / 2, 0.0);
	glVertex3i(b.x - (windowWidth / 4), b.y + wallHeight / 2, 0.0);
	glVertex3i(b.x - (windowWidth / 4), b.y + wallHeight / 2 + windowWidth / 8, 0.0);
	glVertex3i(b.x - (b.x - a.x) / 8, b.y + wallHeight / 2 + windowWidth / 8, 0.0);

	glEnd();


	//smoke tube

	/*glColor3f(0.0, 1.0, 0.0);
	glVertex3f(0.2, 0.5, 0.0);
	glVertex3f(0.8, 0.6, 0.0);*/

	glFlush();
}

int main(int argc, char* argv[])
{
	glutInit(&argc, argv); // khởi tạo tool kit
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);//thiết lập chế độ màn hình
	glutInitWindowSize(800, 800); //thiết lập kích thước cửa sổ
	glutInitWindowPosition(100, 150); // thiết lập vị trí cửa sổ
	glutCreateWindow("Nguyen Duc Quynh"); // mở cửa sổ màn hình
	glutDisplayFunc(drawHouse); // đăng ký hàm vẽ lại
	myInit();
	glutMainLoop(); //lặp vô tận

	return 0;
}
