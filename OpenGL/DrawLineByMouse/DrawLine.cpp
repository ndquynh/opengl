#include <windows.h>
#include <gl/gl.h>
#include  <glut.h>
#include <math.h>


class GLintPoint
{
public:
	GLint x;
	GLint y;
};

class GLColorf
{
public:
	GLfloat r;
	GLfloat b;
	GLfloat g;
};

GLColorf color = { 1.0f, 1.0f, 1.0f };

const int screenWidth = 640;
const int screenHigh = 480;

void myInit()
{
	glClearColor(0.0, 0.0, 0.0, 0.0);
}

void Reshape(int width, int height) {
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, (GLdouble)width, 0, (GLdouble)height);
}

void putPixel(int x, int y, GLColorf c) {
	glPointSize(4.0f);
	glColor3f(c.r, c.g, c.b);
	glBegin(GL_POINTS);
	glVertex2d(x, y);
	glEnd();

	glFlush();
}

void myDisplay() {
	glClear(GL_COLOR_BUFFER_BIT);

	glFlush();
}

void LineMid(GLintPoint A, GLintPoint B, GLColorf c)
{
	int Dx, Dy, p, Const1, Const2;
	int x, y;

	Dx = B.x - A.x;
	Dy = B.y - A.y;

	p = 2 * Dy - Dx;

	Const1 = 2 * Dy;
	Const2 = 2 * (Dy - Dx);

	double m = Dy / Dx;
	// y = mx - b
	// b = mx - y
	// x = (y + b) / m
	//

	x = A.x;
	y = A.y;

	putPixel(x, y, c);
	
	if (m >= 0 && m <= 1) {
		

		for (int i = A.x; i < B.x; i++)
		{
			if (p < 0)
			{
				p += Const1;
			}
			else {
				p += Const2;
				
				if (Dx < 0) {
					y--;
				}
				else
				{
					y++;
				}
				
			}
			if (Dx < 0) {
				x--;
			} 
			else
			{
				x++; 
			}
			
			putPixel(x, y, c);
		}
	}
	else // m > 1 t�nh x theo y
	{

	}
}

int clickCounter = 0;
GLintPoint point1;
GLintPoint point2;

void myMouseFunc(int button, int state, int x, int y)
{

	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
		clickCounter++;

		if (clickCounter % 2 == 0) {
			point2 = { x, screenHigh - y };
			LineMid(point1, point2, color);
		}
		else {
			point1 = { x, screenHigh - y };
			putPixel(point1.x, point1.y, color);
		}


	}
	else if (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN) {
		exit(-1);
	}
}

int main(int argc, char * argv[]) {

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(screenWidth, screenHigh);
	glutInitWindowPosition(100, 150);
	glutCreateWindow("draw line Color");

	myInit();

	glutDisplayFunc(myDisplay);

	glutReshapeFunc(Reshape);

	glutMouseFunc(myMouseFunc);

	glutMainLoop();

	return 0;
}