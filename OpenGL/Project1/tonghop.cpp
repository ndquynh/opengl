#include <windows.h>
#include <gl/gl.h>
#include  <glut.h>
#include <math.h>

class GLIntPoint
{
public:
	GLint x;
	GLint y;
};

GLIntPoint CP;

class GLColorf
{
public:
	GLfloat r;
	GLfloat g;
	GLfloat b;
};

GLColorf color = { 1.0f, 0.4f, 1.0f }; 

void drawLine(GLIntPoint A, GLIntPoint B, GLColorf c) {
	//gluOrtho2D(0.0, 640.0, 0.0, 480.0);
	//glLineWidth(lineWidth);// set befor begin to draw
	glBegin(GL_LINES);
		glVertex2i(A.x, A.y);
		glVertex2i(B.x, B.y);
	glEnd();
}

void myInit()
{
	glClearColor(0.0, 0.0, 0.0, 0.0);
}
int w = 100;

void DrawSquare(GLIntPoint center, int w, GLColorf c) {
	GLIntPoint pos[] = { {-w, w},{ w, w },{ w, -w }, { -w, -w },{ -w, w } };

	for (int i = 0; i < 4; i++) {
		drawLine({ center.x + pos[i].x, center.y + pos[i].y }, { center.x + pos[i + 1].x, center.y + pos[i + 1].y }, c);
	}

	glFlush();
}

void DrawDot(GLIntPoint dot, GLColorf c) {
	glPointSize(2.0);
		glBegin(GL_POINTS);
		glColor3ub(c.r, c.g, c.b);
		glVertex2f(dot.x, dot.y);
	glEnd();
}

void Reshape(int Width, int Height)
{
	glViewport(0, 0, (GLsizei)Width, (GLsizei)Height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, (GLdouble)Width, 0, (GLdouble)Height);
}

void myDisplay()
{ 

	glClear(GL_COLOR_BUFFER_BIT);

	int xMAX = glutGet(GLUT_WINDOW_WIDTH);
	int yMAX = glutGet(GLUT_WINDOW_HEIGHT);

	DrawSquare({ xMAX / 2, yMAX / 2 }, w, color);
	DrawDot({ xMAX / 2, yMAX / 2 }, {1.0f, 0.0f, 0.0f});

	glFlush();
}

void OnKey(unsigned char key, int x, int y) {
	switch (key)
	{
	case '+':
		w += 10; break;
	case '-':
		w -= 10; break;
	case 27:
		exit(0); break;
	default:
		break;
	}
}


int main(int argc, char * argv[])
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(640, 480);
	glutInitWindowPosition(100, 150);
	glutCreateWindow("My first program");
	
	myInit();

	glutDisplayFunc(myDisplay);

	glutReshapeFunc(Reshape);

	glutKeyboardFunc(OnKey);

	glutMainLoop();
	return 0;

}